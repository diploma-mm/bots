const mineflayer = require("mineflayer");

const numOfBots = 24;

const bots = new Map();

for (let i = 0; i < numOfBots; i++) {
    setTimeout(() => {
        const bot = createBot(i);
        bot.once("spawn", () => {
            bot.chat("Hi!");

        });
    }, i * 1000);
}

loop();

function loop(){
    setTimeout(() => {
        loop();
    }, 5000);
}


function createBot(index) {
    const bot = mineflayer.createBot({
        host: "192.168.178.120",
        port: 25565,
        username: "Bot"+index
    })

    bots.set(index, bot);
    return bot;
}



