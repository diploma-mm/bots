const mineflayer = require("mineflayer");

const numOfBots = 12*20;

const bots = new Map();
const isInWorld = new Map();

for (let i = 0; i < numOfBots; i++) {
    setTimeout(() => {
        const bot = createBot(i);
        bot.once("spawn", () => {
            bot.chat("Hi!");
            isInWorld.set(i, false);
            bot.once("spawn", () => {
                let world = !isInWorld.get(i);
                isInWorld.set(i, world);
                if(world){
                    bot.chat("I joined a world!")
                    console.log(bot.username + " loaded into world!")
                }
                else {
                    console.log("Removed from world!")
                }
            })
            loadWorld(i, bot)
        });
    }, i * 1000);
}

loop();

function loop(){
    setTimeout(() => {
        //console.log(isInWorld)
        loop();
    }, 5000);
}

function loadWorld(i, bot) {
    //console.log(bot.username+" loading world!")
    bot.chat("/play")
    setTimeout(() => {
        if(isInWorld.get(i)) return;
        loadWorld(i, bot);
    }, 10000);
}

function createBot(index) {
    const bot = mineflayer.createBot({
        host: "192.168.178.120",
        port: 25577,
        username: "Bot"+index
    })

    bots.set(index, bot);
    return bot;
}



